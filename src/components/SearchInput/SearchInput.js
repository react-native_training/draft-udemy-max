import React, { Component } from "react";
import { View, TextInput, Button, StyleSheet } from "react-native";

class SearchInput extends Component {
  state = {
    schoolName: ""
  };

  schoolNameChangedHandler = val => {
    this.setState({
      schoolName: val
    });
  };

  searchSubmitHandler = () => {
    if (this.state.schoolName.trim() === "") {
      return;
    }

    this.props.onSchoolAdded(this.state.schoolName);
  };

  render() {
    return (
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="School Name"
          value={this.state.schoolName}
          onChangeText={this.schoolNameChangedHandler}
          style={styles.searchInput}
        />
        <Button
          title="Search"
          style={styles.searchButton}
          onPress={this.searchSubmitHandler}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    // flex: 1,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  searchInput: {
    width: "70%"
  },
  searchButton: {
    width: "30%"
  }
});

export default SearchInput;
