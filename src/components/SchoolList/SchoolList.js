import React from 'react';
import { FlatList, StyleSheet } from 'react-native';

import ListItem from '../ListItem/ListItem';

const schoolList = props => {

    return (
        <FlatList 
          style={styles.listContainer}
          data={props.schools}
          renderItem={(info) => (
            <ListItem key={info.item.key} 
              schoolName={info.item.name} 
              schoolImg={info.item.schoolImg} 
              onItemPressed={ () => props.onItemSelected(info.item.key) }
            />
          )} 
          keyExtractor={(item) => item.key.toString() }
        />
    );
};

const styles = StyleSheet.create({
    listContainer: {
      width: "100%"
    }
});

export default schoolList;