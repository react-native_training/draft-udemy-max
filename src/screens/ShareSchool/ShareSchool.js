import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';

import SearchInput from '../../components/SearchInput/SearchInput';
import { addSchool } from '../../store/actions/index';

class ShareSchoolScreen extends Component {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed(event) {
    if (event.buttonId !== 'menuButton') {
      return;
    }
    console.log(event.buttonId);
    // Use the assigned id here
    Navigation.mergeOptions('leftSideDrawer', {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  }

  schoolAddedHandler = schoolName => {
    this.props.onAddSchool(schoolName);
  };

  render() {
    return (
      <View>
        <SearchInput onSchoolAdded={this.schoolAddedHandler} />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAddSchool: schoolName => dispatch(addSchool(schoolName))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ShareSchoolScreen);
