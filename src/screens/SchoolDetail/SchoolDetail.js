import React, { Component } from 'react';
import { View, Text, Image, Button, StyleSheet, TouchableOpacity } from 'react-native';
import { Navigation } from "react-native-navigation";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from "react-redux";
import { deleteSchool } from '../../store/actions/index';

class SchoolDetail extends Component {

  deleteSchoolHandler = () => {
    this.props.onDeleteSchool(this.props.selectedSchool.key);
    Navigation.pop(this.props.componentId);
  }

  render() {
    return (
        <View style={ styles.container }>
          <View>
            <Image source={this.props.selectedSchool.schoolImg} style={ styles.schoolImg } />
            <Text style={ styles.schoolName }>{ this.props.selectedSchool.name }</Text>
          </View>
          <View>
            {/* <Button title="Delete" color="red" onPress={ props.onItemDeleted } /> */}
            <TouchableOpacity onPress={ this.deleteSchoolHandler }>
              <View style={ styles.deleteButton } >
                <Icon size={30} name="md-trash" color="red"/>
              </View>
            </TouchableOpacity>
          </View>
        </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    margin: 20
  },
  schoolImg: {
    width: '100%'
  },
  schoolName: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 28,
    marginBottom: 10
  },
  deleteButton: {
    alignItems: "center"
  }
})

const mapDispatchToProps = dispatch => {
  return {
    onDeleteSchool: (key) => dispatch(deleteSchool(key))
  }
}

export default connect(null, mapDispatchToProps)(SchoolDetail);
