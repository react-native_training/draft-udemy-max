import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';

import SchoolList from '../../components/SchoolList/SchoolList';

class FindSchoolScreen extends Component {

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed(event) {
    if (event.buttonId !== 'menuButton') {
      return;
    }
    console.log(event.buttonId);
    // Use the assigned id here
    Navigation.mergeOptions('leftSideDrawer', {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  }
  
  itemSelectedHandler = key => {
    const selectedSchool = this.props.schools.find(school => school.key === key);
    Navigation.push(this.props.componentId, {
      component: {
        name: 'smart-school.SchoolDetailScreen',
        passProps: {
          selectedSchool
        },
        options: {
          topBar: {
            title: {
              text: selectedSchool.name
            }
          }
        }
      }
    });
  };

  render() {
    return (
      <View>
        <SchoolList
          schools={this.props.schools}
          onItemSelected={this.itemSelectedHandler}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    schools: state.schoolsReducer.schools
  };
};

export default connect(mapStateToProps)(FindSchoolScreen);
