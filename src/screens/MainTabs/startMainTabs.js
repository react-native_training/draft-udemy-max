import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const startTabs = () => {
  Promise.all([
    Icon.getImageSource('md-share-alt', 30, 'pink'),
    Icon.getImageSource('md-map', 30, 'skyblue'),
    Icon.getImageSource('md-menu', 30, 'skyblue')
  ]).then(sources => {
    Navigation.setRoot({
      root: {
        sideMenu: {
          id: 'sideDrawer',
          left: {
            component: {
              id: 'leftSideDrawer',
              name: 'smart-school.SideDrawerScreen'
            }
          },
          center: {
            bottomTabs: {
              children: [
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: 'smart-school.ShareSchoolScreen'
                        }
                      }
                    ],
                    options: {
                      topBar: {
                        leftButtons: [
                          {
                            id: 'menuButton',
                            icon: sources[2]
                          }
                        ],
                        title: {
                          text: 'Share School'
                        }
                      },
                      bottomTab: {
                        text: 'Share School',
                        icon: sources[0],
                        testID: 'SHARE_SCHOOL_TAB_BAR_BUTTON'
                      }
                    }
                  }
                },
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: 'smart-school.FindSchoolScreen'
                        }
                      }
                    ],
                    options: {
                      topBar: {
                        leftButtons: [
                          {
                            id: 'menuButton',
                            icon: sources[2]
                          }
                        ],
                        title: {
                          text: 'Find School'
                        }
                      },
                      bottomTab: {
                        text: 'Find School',
                        icon: sources[1],
                        testID: 'FIND_SCHOOL_TAB_BAR_BUTTON'
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      }
    });
  });
};

export default startTabs;
