import actionTypes  from './actionTypes';

export const addSchool = (schoolName) => {
  return {
    type: actionTypes.ADD_SCHOOL,
    schoolName
  }
}

export const deleteSchool = (key) => {
  return {
    type: actionTypes.DELETE_SCHOOL,
    schoolKey: key
  }
}
