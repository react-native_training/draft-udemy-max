const ADD_SCHOOL = 'ADD_SCHOOL';
const DELETE_SCHOOL = 'DELETE_SCHOOL';

export default actionTypes = {
  ADD_SCHOOL,
  DELETE_SCHOOL
};
