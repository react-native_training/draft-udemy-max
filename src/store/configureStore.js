import { createStore, combineReducers } from 'redux';
import reducer from './reducers/schools';

const rootReducer = combineReducers({
  schoolsReducer: reducer
});

const configureStore = () => {
  return createStore(rootReducer)
}

export default configureStore;
