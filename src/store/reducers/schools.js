import actionTypes from '../actions/actionTypes';

import SchoolImage from '../../assets/images/jassani.png';

initialState = {
  schools: []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_SCHOOL:
      return {
        ...state,
        schools: state.schools.concat({key: Math.random() * 100, name: action.schoolName, schoolImg: SchoolImage})
      }

    case actionTypes.DELETE_SCHOOL:
      return {
        ...state,
        schools: state.schools.filter(school => school.key !== action.schoolKey)
      }

    default:
      return state;
  }
}

export default reducer;
