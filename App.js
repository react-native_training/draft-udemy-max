import { Navigation } from 'react-native-navigation';
import AuthScreen from './src/screens/Auth/Auth';
import { Provider } from 'react-redux';

import ShareSchoolScreen from './src/screens/ShareSchool/ShareSchool';
import FindSchoolScreen from './src/screens/FindSchool/FindSchool';
import SchoolDetailScreen from './src/screens/SchoolDetail/SchoolDetail';
import SideDrawerScreen from './src/screens/SideDrawer/SideDrawer';
import configureStore from './src/store/configureStore';

const store = configureStore();

// register screen before use them
Navigation.registerComponentWithRedux(
  'smart-school.AuthScreen',
  () => AuthScreen,
  Provider,
  store
);
Navigation.registerComponentWithRedux(
  'smart-school.ShareSchoolScreen',
  () => ShareSchoolScreen,
  Provider,
  store
);
Navigation.registerComponentWithRedux(
  'smart-school.FindSchoolScreen',
  () => FindSchoolScreen,
  Provider,
  store
);
Navigation.registerComponentWithRedux(
  'smart-school.SchoolDetailScreen',
  () => SchoolDetailScreen,
  Provider,
  store
);
Navigation.registerComponent(
  'smart-school.SideDrawerScreen',
  () => SideDrawerScreen
);

// (Common gotcha: Be sure not to run setRoot before registerAppLaunchedListener() has fired!)
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      sideMenu: {
        id: 'sideDrawer',
        left: {
          component: {
            id: 'leftSideDrawer',
            name: 'smart-school.SideDrawerScreen'
          }
        },
        center: {
          stack: {
            children: [
              {
                component: {
                  name: 'smart-school.AuthScreen'
                }
              }
            ],
            options: {
              topBar: {
                title: {
                  text: 'Login'
                }
              }
            }
          }
        }
      }
    }
  });
});
